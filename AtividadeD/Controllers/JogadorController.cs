using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace SatcBackend.Controllers;

[ApiController]
[Route("/")]
public class JogadorController : ControllerBase
{
    [HttpGet("GerarJogador")]
    public string GerarJogador()
    {
        return JogadorGerador.NovoJogador().GetDescricao();
    }

    [HttpGet("GerarJogadorJson")]
    public Jogador GerarJogadorJson()
    {
        Jogador jogador = JogadorGerador.NovoJogador();
        return JogadorGerador.NovoJogador();
    }
}
