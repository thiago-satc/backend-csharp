﻿using SatcBackend;
using System;
using System.Collections;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SatcBackend;

public static class JogadorGerador
{
    private const string uri = "https://venson.net.br/resources/data";
    private const string nome_uri      = uri + "/nomes.txt";
    private const string sobreNome_uri = uri + "/sobrenomes.txt";
    private const string posicao_uri   = uri + "/posicoes.txt";
    private const string clube_uri     = uri + "/clubes.txt";

    private static string[]? Nomes;
    private static string[]? Sobrenomes;
    private static string[]? Posicoes;
    private static string[]? Clubes;

    private static string retornaElementoAleatorio(string[] elementos)
    {
        int indice = new Random().Next(0, elementos.Length);
        return elementos[indice];
    }

    private static string[] chamar(string uri)
    {
        HttpClient client = new();
        HttpResponseMessage result = client.GetAsync(uri).Result;
        string value = result.Content.ReadAsStringAsync().Result;
        return value.Split("\n");
    }

    private static int gerarIdadeAleatoria(int minima, int maxima)
    {
        if (minima < 0) minima = 0;
        if (maxima > 200) maxima = 200;
        return new Random().Next(minima, maxima + 1);
    }

    public static void RecuperarDadosJogadores()
    {
        Nomes = chamar(nome_uri);
        Sobrenomes = chamar(sobreNome_uri);
        Posicoes = chamar(posicao_uri);
        Clubes = chamar(clube_uri);
        
        for(int i=0; i<Posicoes.Length; i++) 
        {
            Posicoes[i] = Posicoes[i].Replace("\"", "").Replace(",", "");
        }
    }

    public static Jogador NovoJogador()
    {
        string nome = retornaElementoAleatorio(Nomes);
        string sobrenome = retornaElementoAleatorio(Sobrenomes);
        string posicao = retornaElementoAleatorio(Posicoes);
        string clube = retornaElementoAleatorio(Clubes);
        Jogador jogador = new(nome, sobrenome, gerarIdadeAleatoria(17, 40), posicao, clube);
        return jogador;
    }

}