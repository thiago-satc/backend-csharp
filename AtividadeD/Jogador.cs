﻿using System.ComponentModel;

namespace SatcBackend;

public class Jogador
{
    public string nome { get; set; }
    public string SobreNome { get; set; }
    public int Idade { get; set; }
    public string Posicao { get; set; }
    public string Clube { get; set; }

    public Jogador(string nome, string sobreNome, int idade, string posicao, string clube)
    {
        this.nome = nome;
        this.SobreNome = sobreNome;
        this.Idade = idade;
        this.Posicao = posicao;
        this.Clube = clube;
    }

    public String GetDescricao()
    {
        return string.Format($"{nome} {SobreNome} é um futebolista brasileiro de {Idade} anos que atua como {Posicao}. Atualmente defende o {Clube}.");
    }
}